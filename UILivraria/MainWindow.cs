﻿using System;
using Entidades;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel) => Build();

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnButton1Clicked(object sender, EventArgs e)
    {

        string nome = entry1.Text;
        string email = entry2.Text;
        string cpf = entry3.Text;

        Autor autor = new Autor(nome, email, cpf);
        // so pra verificar se realmente ta funcionando
        entry4.Text = autor.Nome + ", " + autor.Email + ", " + autor.Cpf;

    }

    protected void OnButton1Pressed(object sender, EventArgs e)
    {
    }
}
