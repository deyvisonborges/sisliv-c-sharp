﻿using Entidades;
using Produtos;

namespace ProdutosLivro
{
    public class Ebook : Livro
    {
        private string marcaDagua;
        public string MarcaDagua
        {
            get => this.marcaDagua;
            set => this.marcaDagua = value;
        }

        public Ebook(Autor autor) : base(autor) { }
        public override bool AplicaDesconto(double porcentagem)
        {
            if (porcentagem > 0.15)
            {
                return false;
            }

            double desconto = this.Valor -= this.Valor * porcentagem;
            this.Valor = this.Valor - desconto;
            return true;
        }
    }
}