﻿using Entidades;
using Produtos;

namespace ProdutosLivro
{
    public class LivroFisico : Livro
    {
        public LivroFisico(Autor autor) : base(autor) { }

        public double TaxaImpressao(double taxa)
        {
            return this.Valor * taxa;
        }

        public override bool AplicaDesconto(double porcentagem)
        {
            if (porcentagem > 0.3)
            {
                return false;
            }
            double desconto = this.Valor -= this.Valor * porcentagem;
            this.Valor = this.Valor - desconto;
            return true;
        }
    }
}