﻿using Entidades;
using Produtos;

namespace ProdutosLivro
{
    public class MiniLivro : Livro
    {
        public MiniLivro(Autor autor) : base(autor) { }
        public override bool AplicaDesconto(double porcentagem) => false;
    }
}