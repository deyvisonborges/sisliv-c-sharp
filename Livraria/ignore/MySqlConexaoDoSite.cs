﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

public class Con
{
    public void Main(string[] args)
    {
        string connectionString =
           "Server=localhost;" +
           "Database=livraria;" +
           "User ID=root;" +
           "Password=220592;" +
           "Pooling=false";
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);
        dbcon.Open();
        IDbCommand dbcmd = dbcon.CreateCommand();

        string sql =
            "SELECT NOME, EMAIL " +
            "FROM AUTOR";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            string FirstName = (string)reader[0];
            string LastName = (string)reader["EMAIL"];
            Console.WriteLine("Name: " +
                  FirstName + " " + LastName);
        }
        // clean up
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbcon.Close();
        dbcon = null;
    }
}