﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Conexao
{
    public class DAOMySql : IDbConnection
    {
        private IDbConnection db = new MySqlConnection(
           "Server=localhost;" +
           "Database=livraria;" +
           "User ID=root;" +
           "Password=220592;" +
           "Pooling=false"
        );

        // implementações da interface IDbCommand
        public void Open() => db.Open();
        public IDbCommand CreateCommand() => db.CreateCommand();
        public void Dispose() => db.Dispose();
        public void Close() => db.Close();
        public int ConnectionTimeout => db.ConnectionTimeout;
        public string Database => db.Database;
        public ConnectionState State => db.State;
        public string ConnectionString { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IDbTransaction BeginTransaction() => db.BeginTransaction();
        public IDbTransaction BeginTransaction(IsolationLevel il) => db.BeginTransaction(il);
        public void ChangeDatabase(string databaseName) => db.ChangeDatabase(databaseName);

        // conexao
        public void Conexao(string sql)
        {
            try
            {
                db.Open();
                IDbCommand dbcmd = db.CreateCommand();
                dbcmd.CommandText = sql;
                IDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < sql.Length; i++)
                    {
                        var sqlen = (string)reader[i];
                        Console.WriteLine(sqlen);
                    }
                }
                reader.Close();
                reader = null;
                dbcmd.Dispose();
                dbcmd = null;
                db.Close();
                db = null;
            }
            catch (System.IndexOutOfRangeException)
            {
                return;
            }
        }

    }
}