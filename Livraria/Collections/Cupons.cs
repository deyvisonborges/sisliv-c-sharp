﻿namespace Collections
{
    public class Cupons
    {
        private readonly string[] _cupons;
        private int count;

        // construtor
        public Cupons(int posicoes)
        {
            this._cupons = new string[posicoes];
            this.count = 0;
        }

        // adiciona cupom
        public void AddCupom(string cupom)
        {
            this._cupons[this.count] = cupom;
            this.count++;
        }

        // remove, caso exista
        public void RemoveCupom(string cupomARemover)
        {
            for (int i = 0; i < this._cupons.Length; i++)
            {
                var jaExiste = this._cupons[i];
                if (jaExiste == cupomARemover)
                {
                    this._cupons[i] = null;
                    break;
                }
                this.count--;
            }
        }

        // verifica se existe
        public bool VerificaCupom(string cupomUsado)
        {
            var flag = false;
            for (int i = 0; i < this._cupons.Length; i++)
            {
                var jaExiste = this._cupons[i];
                if (jaExiste == cupomUsado)
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        // print
        public void Print()
        {
            for (int i = 0; i < this._cupons.Length; i++)
            {
                if(this._cupons[i] != null)
                {
                    System.Console.WriteLine(_cupons[i]);
                }
            }
        }
    }
}