﻿namespace Entidades
{
    public class Editora
    {
        // atributos
        private string nomeFantasia;
        private string razaoSocial;
        private string cnpj;

        // construtor
        public Editora(string nomeFantasia, string razaoSocial, string cnpj)
        {
            this.nomeFantasia = nomeFantasia;
            this.razaoSocial = razaoSocial;
            this.cnpj = cnpj;
        }

        // getters
        public string NomeFantasia { get => this.nomeFantasia;  }
        public string RazaoSocial { get => this.razaoSocial; }
        public string Cnpj { get => this.cnpj; }
    }
}