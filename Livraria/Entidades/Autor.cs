﻿namespace Entidades
{
    public class Autor
    {
        // atributos
        private string nome;
        private string email;
        private string cpf;

        // construtor
        public Autor(string nome, string email, string cpf) 
        {
            this.nome = nome;
            this.email = email;
            this.cpf = cpf;
        }

        // getters
        public string Nome { get => this.nome; }
        public string Email { get => this.email; }
        public string Cpf { get => this.cpf; }

    }
}