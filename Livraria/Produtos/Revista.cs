﻿using Entidades;

namespace Produtos
{
    public class Revista
    {
        private string nome;
        private string descricao;
        private double valor;

        public string Nome
        {
            get => nome;
            set => nome = value;
        } // NOME

        public string Descricao
        {
            get => descricao;
            set => descricao = value;
        } // DESCRICAO

        public double Valor
        {
            get => valor;
            set => valor = value;
        } // VALOR

        public Editora Editora { get; set; } // EDITORA

        public bool AplicaDesconto(double porcentagem)
        {
            if (porcentagem > 0.1)
            {
                return false;
            }
            double desconto = this.valor * porcentagem;
            this.valor = this.valor - desconto;
            return true;
        }
    }

}