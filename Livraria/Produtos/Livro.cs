﻿using Entidades;

namespace Produtos
{
    public abstract class Livro
    {
        private string nome;
        private string descricao;
        private double valor;
        private string isbn;

        protected Livro(Autor autor)
        {
            this.Autor = autor;
            this.isbn = "0-0000-000-00";
        }

        public Autor Autor { get; /* utilizei a propriedade 'auto' */ }

        public string Nome 
        { 
            get => this.nome; 
            set => this.nome = value; 
        } // NOME

        public string Descricao 
        { 
            get => this.descricao; 
            set => this.descricao = value; 
        } // DESCRICAO

        public double Valor
        {
            get => this.valor;
            set => this.valor = value;
        } // VALOR

        public string Isbn
        {
            get => isbn;
            set => this.isbn = value;
        } // ISBN

        public abstract bool AplicaDesconto(double porcentagem);
    }
}